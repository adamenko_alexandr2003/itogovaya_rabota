#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

using namespace sf;
using namespace std;

RenderWindow window;
Image image_sun; //создать изображение Солнца
Texture texture_sun; //создать текстуру Солнца
Sprite sprite_sun; //создать спрайт Солнца
Image image_earth; //создать изображение Земли
Texture texture_earth; //создать текстуру Земли
Sprite sprite_earth; //создать спрайт Земли

struct Point
{
    int x;
};

int main()
{
    window.create(VideoMode(1000, 1000), "\0"); // создание окна

    //параметры Солнца
    image_sun.loadFromFile("Sun.png");
    texture_sun.loadFromImage(image_sun);
    sprite_sun.setTexture(texture_sun);
    sprite_sun.setPosition(425, 400);
    sprite_sun.setScale(0.55, 0.55);

    //параметры Земли
    image_earth.loadFromFile("earth.png");
    texture_earth.loadFromImage(image_earth);
    sprite_earth.setTexture(texture_earth);
    sprite_earth.setPosition(250, 250);
    sprite_earth.setScale(0.12, 0.12);

    Point clock;

    clock.x = 0;

    while (window.isOpen())
    {
        Event event;
        while (window.pollEvent(event))
        {
            if (event.type == Event::Closed)
                window.close();
        }
        

        sprite_earth.setRotation(clock.x);

        window.clear();
        window.draw(sprite_earth);
        window.draw(sprite_sun);
        window.display();

        this_thread::sleep_for(22ms);
        clock.x++;
    }

    return 0;
}
