﻿#include <iostream>
#include "Библиотека.h"

using fun::Mat33d;
using fun::Vec3d;

int main()
{
	Mat33d A({ {
		{1,2,3},
		{4,5,6},
		{7,8,6}
	} });

	Vec3d B({ {
		{1},
		{5},
		{0}
	} });

	std::cout << A.inv() << std::endl;
	std::cout << A.det() << std::endl;
	std::cout << A.slau(B) << std::endl;
	std::cout << A.tran() << std::endl;
}
